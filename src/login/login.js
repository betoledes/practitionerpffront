import React, {Component} from 'react';
import { Form, FormGroup, Label, Input, Button, Alert } from "reactstrap";
import Header from '../components/headers/index';

class Login extends Component {

    constructor(props) {
        super(props)
        console.log(this.props)
        this.state = {
            message : this.props.location.state?this.props.location.state.message: '',
        };
    }

    signIn = () => {
        
        const data = {email: this.email, password: this.password};
        const requestInfo = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
        };

        fetch('http://localhost:3003/api/login', requestInfo)
        .then(response => {
            if(response.ok) {
                return response.json()
            }

            throw new Error("Datos incorrectos...");
        })
        .then(token => {
            localStorage.setItem('token', token);
            localStorage.setItem('userId', token.user._id);
            this.props.history.push("/Usuarios");
            return;
        })
        .catch(e => 
            this.setState({message: e.message}));
    }

    render() {
        return(
            <div className="col-md-5">
                <img src="images/BBVA.png" className="logo" alt=""/>
                <Header title="Acceso" />
                <hr className="my-3"/>
               {
                   this.state.message !== ''?(
                        <Alert color="danger" className="text-center">{this.state.message}</Alert>
                   ):''
               }
                <br />
                <Form>
                    <FormGroup>
                        <Label for="email">Email</Label>
                        <Input type="text" id="email" onChange={e => this.email = e.target.value} placeholder="Ingresa tu correo" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Contraseña</Label>
                        <Input type="password" id="password" onChange={e => this.password = e.target.value} placeholder="Ingresa tu contraseña" />
                    </FormGroup>
                    <div className="text-center">
                        <Button color="primary" onClick={this.signIn}>Ingresar</Button>
                    </div>                    
                </Form>
            </div>
        );
    }
}

export default Login;