import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import PrivateRoute from './auth'
import Login from './login/login'
import Users from './users/index'
import UpdateCard from './users/updateCard'
import AddCard from './users/addCard'
import Logout from './users/logout'

const Routes = () => (
    <Router>
        <Switch>
            <Route exact path="/" component={Login}/>
            <PrivateRoute path="/Usuarios" component={Users}/>
            <PrivateRoute path="/modificarTarjeta" component={UpdateCard}/>
            <PrivateRoute path="/agregarTarjeta" component={AddCard}/>
            <Route exact path="/logout" component={Logout}/>
        </Switch>
    </Router>
);

export default Routes;