import React, { Component } from 'react';
import { Table, Button, Alert } from 'reactstrap';
import { Link } from 'react-router-dom';
import Header from '../components/headers'

class Users extends Component {
    constructor() {
        super();
        this.state = {
            user: [],
            data: {},
            message: {
                text: '',
                alert: ''
            }
        };
    }



    componentDidMount() {
        const token = localStorage.getItem('token');
        const userId = localStorage.getItem('userId')
        fetch(`http://localhost:3003/api/users/${userId}`, { headers: new Headers({ 'Authorization': `Bearer ${token}` }) })
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Ha ocurrido un error...")
            })
            .then(user => this.setState({ user: user.creditCard, data: user }))
            .catch(e => console.log(e));
    }

    deleteCard = (id) => {
        fetch(`http://localhost:3003/api/users/${this.state.data._id}/card/${id}`, { method: 'DELETE' })
            .then(response => response.json())
            .then(rows => {
                this.setState({ message: { text: 'Tarjeta eliminada', alert: 'danger' } });
            })
            .catch(e => console.log(e))
    }

    updateCard = (id) => {
        console.log(id)
        localStorage.setItem('cardId', id)
        this.props.history.push("/modificarTarjeta");
    }

    render() {
        return (
            <div>
                <img src="images/BBVA.png" className="logoAlternative" alt="" />
                <Header title="Usuarios" />
                <hr className="my-3" />
                <Table striped>
                    <thead>
                        <tr>
                            <th>Id Cliente</th>
                            <th>Nombre</th>
                            <th>Tarjetas</th>
                            <th>Saldo</th>
                            <th>Ejecutivo</th>
                            <th>Correo</th>
                            <th>Acciones</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.user.map((card, i) =>
                            <tr key={i}>
                                <td>{card.customerId}</td>
                                <td>{card.customerName}</td>
                                <td>{card.creditCardCategory}</td>
                                <td>{card.balance}</td>
                                <td>{this.state.data.username}</td>
                                <td>{this.state.data.email}</td>
                                <td><div className="text-center">
                                    <Button color="danger" onClick={e => this.deleteCard(card._id)}>Eliminar</Button>
                                </div></td>
                                <td><div className="text-center">
                                    <Button color="info" onClick={e => this.updateCard(card._id)}>Editar</Button>
                                </div></td>
                            </tr>
                        )}
                    </tbody>
                </Table>
                {
                    this.state.message.text !== '' ? (
                        <Alert color={this.state.message.alert} className="text-center"> {this.state.message.text} </Alert>
                    ) : ''
                }
                <div className="text-center">
                    <Link to="/agregarTarjeta" className="btn btn-primary" >Agregar Tarjeta</Link>
                </div>
                <br />
                <div className="text-center">
                    <Link to="/logout" className="btn btn-danger" >Cerrar Sesión</Link>
                </div>
            </div>
        )
    }

}

export default Users;