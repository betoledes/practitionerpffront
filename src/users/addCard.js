import React, { Component } from 'react';
import { Form, FormGroup, Label, Input, Button, Alert } from "reactstrap";
import { Link } from 'react-router-dom';
import Header from '../components/headers'


class AddCard extends Component {

    constructor() {
        super();
        this.state = {
            message: {
                text: '',
                alert: ''
            }
        };
    }

    addCard = () => {
        const data = {cardNumber: this.cardNumber, customerId: this.customerId, customerName: this.customerName, cardType: this.cardType, creditCardCategory: this.creditCardCategory, balance: this.balance}
        const request = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
        };

        const userId = localStorage.getItem('userId')
        console.log(userId)
        fetch(`http://localhost:3003/api/users/${userId}/assignCard`, request)
        .then(response => {
            if(response.ok) {
                return response.json()
            }

            throw new Error("Ha habido un error...");
        })
        .then(rows => {
            this.setState({ message: { text: 'Tarjeta agregada', alert: 'info' } });
        })
    }

    render() {
        return (
            <div className="col-md-5">
                <img src="images/BBVA.png" className="logoAlternative" alt="" />
                <Header title="Tarjetas" />
                <hr className="my-3" />
                <br />
                <Form>
                    <FormGroup>
                        <Label for="cardNumber">Número de Tarjeta</Label>
                        <Input type="text" id="cardNumber" onChange={e => this.cardNumber = e.target.value} placeholder="Ingresa el número de tarjeta" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="customerId">Id del Cliente</Label>
                        <Input type="text" id="customerId" onChange={e => this.customerId = e.target.value} placeholder="Ingresa Id del cliente" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="customerName">Nombre del Cliente</Label>
                        <Input type="text" id="customerName" onChange={e => this.customerName = e.target.value} placeholder="Ingresa el nombre del cliente" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="cardType">Tipo de tarjeta</Label>
                        <Input type="text" id="cardType" onChange={e => this.cardType = e.target.value} placeholder="Ingresa tipo de tarjeta (Débito/Crédito)" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="creditCardCategory">Tipo de categoría</Label>
                        <Input type="text" id="creditCardCategory" onChange={e => this.creditCardCategory = e.target.value} placeholder="Ingresa categoría de tarjeta (Azul/Oro...)" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="balance">Saldo</Label>
                        <Input type="text" id="balance" onChange={e => this.balance = e.target.value} placeholder="Ingresa saldo de tarjeta" />
                    </FormGroup>
                    {
                        this.state.message.text !== '' ? (
                            <Alert color={this.state.message.alert} className="text-center"> {this.state.message.text} </Alert>
                        ) : ''
                    }
                    <div className="text-center">
                        <Button color="primary" onClick={this.addCard}>Agregar</Button>
                    </div>
                </Form>
                <br/>
                <div className="text-center">
                    <Link to="/Usuarios" className="btn btn-secondary">Regresar</Link>
                </div>
            </div>
        )
    }

}

export default AddCard
